#!/usr/bin/env python3
from icon_data import plevDataFiles, slevDataFiles, tinvDataFiles, timeGapHours
from icon_wind import readLatLonData, windData, dzPressureLevels, latlonBounds
from localize_rescale import scaleGustsAndReport, localizeMeteoAndReport, utmToLatLng
import numpy as np
import argparse
'''
# - - - - - - - - - - - - - - - - - - #
Python script for generating local wind predictions on selected sites along Spanish A2 motorway 
or along Egnatia Odos M2 highway near the Metsovo bridge in Greece. The wind predictions are generated utilizing precomputed LES results which are scaled according to open-access ICON-EU numerical wheather prediction model results for high altitude wind. 

The script has been updated to also provide a localized weather prediction concerning temperature, 
precipitation and relative humidity.

The user should at least provide the location indicator (-l) with either {'S','G'} (for Spain 
and Greece respectively). Optionally, the user can also specify the most suitable pressure 
level (-pl) for scaling height from the following choices {850,825,800,775} hPa. 
By default, -pl 825 is used for the Spanish pilot site and -pl 800 for the Greek site.

Run the script (in Linux terminal) with help: 
>> python3 wind_predict.py -h 

Example run for Spainish pilot site using 775hPa pressure level as scaling height:
>> python3 wind_predict.py -l S -pl 775 

Example run for Greek pilot site (using by default 800hPa pressure level as scaling height):
>> python3 wind_predict.py -l G 

By default, the outputs are written into wind_record.dat and meteo_record.dat ASCII files in 
comma separated variable (csv) format.

Author: Mikko Auvinen, mikko.auvinen@fmi.fi, Finnish Meteorological Institute, Helsinki, Finland
Project: PANOPTIS

# - - - - - - - - - - - - - - - - - - #
For developers:

The script requires access to 
1) the internet (for obtaining the recent ICON-EU forecast)

2) included Python modules: icon_data, icon_wind and localize_rescale. Should be placed within 
the same directory or included in PYTHONPATH environment variable. Also the following python3 
libraries are required: netCDF4, argparse, numpy, sys, bz2, request, cdo and time.

3) Access to precomputed LES model output datasets (two for each pilot site) that are stored 
within the follwing directories:

./U1D-$L/Umag_$WDR.dat
where $L = {S,G} : S=Spain A2 Motorway, G=Greece Metsovo Bridge
and $WDR = {015, 045, 075, ... , 285, 315, 345} the mean wind direction in 30 deg sectors.
File contain z, U(z) values for mean wind. Format is ASCII txt. 

./UGUST-$L/UG_$NST_$WDR.npz
where $L and $WDR as above while for [S]pain $NST = {N05,N06, ..., N09, N10} and for 
[G]reece $NST = {ID01,ID02, ..., ID08, ID09} indicates the PALM LES model specific subdomain 
label where detailed wind data is collected. These files contain the relevant, localized and 
scalable wind information.

NOTE: no clean-up is done by the script. All .grib2 and .bz2 files could (should!) be removed 
periodically or after each run. Enjoy!
# - - - - - - - - - - - - - - - - - - #
'''

#==========================================================#
parser = argparse.ArgumentParser(prog='wind_predict.py')
parser.add_argument("-fo", "--fileout", type=str,default='wind_record.dat',\
  help="Name of the output ASCII file containing a record of the wind information. Default=wind_record.dat")
parser.add_argument("-fm", "--filemet", type=str,default='meteo_record.dat',\
  help="Name of the output ASCII file containing a record of the meteorological nowcast. Default=meteo_record.dat")
parser.add_argument("-l", "--location", type=str, choices=['S','G'], required=True,\
  help="Location indicator. [S]=Spain A2 Motorway, [G]=Greece Egnatia Odos (Metsovo Bridge).")
parser.add_argument("-dt", "--dtime", type=int, choices=[6,9], default=6,\
  help="Time offset in hours (when forecast for this moment is published)")
parser.add_argument("-pl", "--plev", type=int,choices=[775,800,825,850], default=None,\
  help="Pressure level (hPa) from which data is collected. Defaults: [S]->825, [G]->800")
parser.add_argument("-v","--verbose", action="store_true", default=False,\
  help="Run with extra output to screen.")
args = parser.parse_args()
#==========================================================#

fileout  = args.fileout
filemet  = args.filemet
plev     = args.plev
dtime    = args.dtime
location = args.location
verbose  = args.verbose

# - - - - - - - - - - - - - - - - - - #

latb, lonb = latlonBounds( location )

if( location == 'S' ): 
  modelRotation = 40.
  if(plev is None): plev = 825    # For now, it is best to 
  zone = 30.
else: #( location == 'G' ):
  modelRotation = -20.  # Deg
  if(plev is None): plev = 800
  zone = 34.
  
#==========================================================#
# NOTE: Variable names within the files are often different from the data strings used in file names. 
# Wind components and temperature
data_plev   = ['v','u','t']
var_plev    = ['v','u','t']

# wind and temperature at 2 m above ground
# surface pressure [ps], dew point temperature [td_2m]
data_single = ['t_2m', 'ps', 'tmax_2m', 'tmin_2m', 'relhum_2m','tot_prec']
var_single  = ['2t'  , 'sp', 'TMAX_2M', 'TMIN_2M', '2r'       , 'tp'     ]   

# Surface height
data_inv    = ['hsurf']
var_inv     = ['HSURF']

fplevDict = plevDataFiles( data_plev  , plev, dtime )
fslevDict = slevDataFiles( data_single, dtime )
finvDict  = tinvDataFiles( data_inv, dtime )

#==========================================================#
# Weather treatment 
rh_2m,    X, Y = readLatLonData('2r'     , fslevDict['relhum_2m'], latb, lonb )
t_2m,     X, Y = readLatLonData('2t'     , fslevDict['t_2m'],      latb, lonb )
tmin_2m,  X, Y = readLatLonData('TMIN_2M', fslevDict['tmin_2m'],   latb, lonb )
tmax_2m,  X, Y = readLatLonData('TMAX_2M', fslevDict['tmax_2m'],   latb, lonb )
hs,       X, Y = readLatLonData('HSURF'  , finvDict['hsurf'],      latb, lonb )
pr,       X, Y = readLatLonData('tp'     , fslevDict['tot_prec'],  latb, lonb )


# Localize meteorological data and compile a report 
nhours = timeGapHours( dtime )
meteo_info, meteo_header, hsbins \
  = localizeMeteoAndReport(nhours, hs, pr, rh_2m, tmax_2m, tmin_2m, t_2m)

#==========================================================#
# Compute wind speed and direction for pressure level plev
u,     X, Y = readLatLonData('u',   fplevDict['u'],     latb, lonb)
v,     X, Y = readLatLonData('v',   fplevDict['v'],     latb, lonb )
umag, wdir = windData( u, v )
u = v = None

if( verbose ):
  # Surface wind information is for research purposes, not meant for operational runs
  data_w10m = [ 'u_10m', 'v_10m']
  var_w10m  = [ '10u'  , '10v'  ]
  fw10Dict  = slevDataFiles( data_w10m, dtime )
  
  # Compute wind speed and direction for 10 m elevation (testing only)
  u_10m, X, Y = readLatLonData('10u', fw10Dict['u_10m'], latb, lonb )
  v_10m, X, Y = readLatLonData('10v', fw10Dict['v_10m'], latb, lonb )
  fw10Dict = None
  umag_10m, wdir_10m  = windData( u_10m, v_10m )
  print(' mean: u_10m = {}, v_10m = {} '.format(np.mean(u_10m), np.mean(v_10m)))
  print(' mean: umag_10m = {}, wdir = {} '.format(np.mean(umag_10m), np.mean(wdir_10m)))
  u_10m = v_10m = None

#==========================================================#
# Determine the distance between the ground and the pressure level plev
t,     X, Y = readLatLonData( 't',    fplevDict['t'],    latb, lonb  )
p_2m,  X, Y = readLatLonData( 'sp',   fslevDict['ps'],   latb, lonb  )

# Height of the reference pressure level using hypsometric equation
Zref = dzPressureLevels( t, t_2m, plev, p_2m, hs )
t = p_2m = None

#==========================================================#
# To be on the concervative side, we choose the 75th percentile for the reference wind.
Uref = np.percentile( umag , 75 ) 

#==========================================================#
# Wind direction has to take into account the model rotation
wd_eff = np.mean( wdir ) + modelRotation
if( wd_eff >= 360. ): wd_eff -= 360.

WDref  = int((wd_eff // 30) * 30) + 15

#==========================================================#
# Perform final scaling and present the data 

wind_report, UtmE, UtmN = scaleGustsAndReport( Uref, Zref, location, WDref, verbose )

meteo_report = meteo_header
for ue, un in zip(UtmE, UtmN):
  lat,lon = utmToLatLng( zone, ue, un )
  dy = np.abs(Y[:,0]-lat); dx = np.abs(X[0,:]-lon)
  i = np.where( np.min( dx )== dx)[0][0]
  j = np.where( np.min( dy )== dy)[0][0]
  Hji = hs[j,i]; #print(' H={}'.format(Hji))
  k = np.where( (Hji>hsbins[:-1])*(Hji<hsbins[1:]) )[0][0]
  meteo_report += '{:11.3f}, {:11.3f}, '.format(ue,un)+meteo_info[k]


if( verbose ):
  print(' ICON reference height,     Zref = {:4.1f} m'.format(Zref))
  print(' ICON reference wind speed, Uref =  {:4.1f} m/s'.format(Uref))
  print(' ICON reference wind direction (0: north, 90: east) =  {:3.0f} deg\n'.format(WDref))
  print(wind_report)
  print(meteo_report)


# Write wind and meteo reports into their respective files.
with open(fileout, 'w') as fo:
  fo.write(wind_report)

with open(filemet, 'w') as fo:
  fo.write(meteo_report)
