#!/usr/bin/env python3
import numpy as np
import time
import bz2 
import requests
import cdo
from os import path

#==========================================================#
def nearestTimeStamp( h, dt=None ):
  ts = ( h // 3 ) * 3
  
  if( dt is None ): dh = None
  else:             dh = (h+dt)-ts  # := (current time) - time stamp
  
  return ts, dh

#==========================================================#

def timeStamp(dth):
  secs  = time.time() - float(dth)*3600.  # time dth hours ago
  tinfo = time.localtime(secs)            # current time in clock format
  yy = tinfo.tm_year; mm = tinfo.tm_mon   # current year, month
  dd = tinfo.tm_mday; hh = tinfo.tm_hour  # current day, hour 
  return (yy,mm,dd,hh)

#==========================================================#

def timeGapHours(dth):
  _, _, _, hh = timeStamp( dth )
  _, dh = nearestTimeStamp( hh , dth )
  
  return dh

#==========================================================#

def pressureLevelFileAndPath( var, yy, mm, dd, hh, plev, dt):
  
  th, dh = nearestTimeStamp( hh, dt )
  
  fname = 'icon-eu_europe_regular-lat-lon_pressure-level_{}{:02d}{:02d}{:02d}_{:03d}_{}_{}.grib2'.format(yy,mm,dd,th,dh,plev,var.upper())
  path  = 'https://opendata.dwd.de/weather/nwp/icon-eu/grib/{:02d}/{}/{}.bz2'.format(th,var,fname)
  
  return fname, path

#==========================================================#

def singleLevelFileAndPath( var, yy, mm, dd, hh , dt):
  
  th, dh = nearestTimeStamp( hh, dt )
  
  fname = 'icon-eu_europe_regular-lat-lon_single-level_{}{:02d}{:02d}{:02d}_{:03d}_{}.grib2'.format(yy,mm,dd,th,dh,var.upper())
  path  = 'https://opendata.dwd.de/weather/nwp/icon-eu/grib/{:02d}/{}/{}.bz2'.format(th,var,fname)
  
  return fname, path

#==========================================================#

def timeInvariantLevelFileAndPath( var, yy, mm, dd, hh ):
  
  th, dh = nearestTimeStamp( hh )
  
  fname = 'icon-eu_europe_regular-lat-lon_time-invariant_{}{:02d}{:02d}{:02d}_{}.grib2'.format(yy,mm,dd,th,var.upper())
  path  = 'https://opendata.dwd.de/weather/nwp/icon-eu/grib/{:02d}/{}/{}.bz2'.format(th,var,fname)
  
  return fname, path

#==========================================================#

def ddDataFile(f, p, var):
  '''
  Download and decompress the data file. 
  Here, bz2.BZ2File takes the longest time. Optimizations should be targeted there.
  '''
  fn = '{}.bz2'.format(f)
  if( not path.exists( fn ) ):
    r = requests.get(p, allow_redirects=True)
    size = open(fn, 'wb').write(r.content)
    #print(' Downloaded file {} of size {}'.format(f,size))
  
  with bz2.BZ2File('./{}'.format(fn)) as fh:
    content = fh.read()
  
  fileout = '{}.grib2'.format(var.upper())
  with open(fileout, "wb") as fo:
    unused = fo.write(content)
  
  return fileout

#==========================================================#

def grib2ToNetCDF(f, var, plev=None):
  '''
  Convert grib2 file to netCDF using freely available cdo library. 
  cmd = 'cdo -f nc copy {}  {}.nc'.format(f,var)
  '''
  if( plev is None ):
    fstr = '{}.nc'.format(var)
  else:
    fstr = '{}_{:3d}.nc'.format(var,plev)
  
  C = cdo.Cdo()
  fout = C.copy(input=f, options='-f nc', output=fstr)
  #print(' Converted {} --> {}'.format(f,fout))
  
  return fstr

#==========================================================#

def plevDataFiles( varList, plev, dtime ):
  y,m,d,h = timeStamp(dtime)
  
  fnameDict = dict()
  for vstr in varList:
    fv, pv = pressureLevelFileAndPath( vstr, y, m, d, h, plev, dtime )
    fvo    = ddDataFile(fv, pv, vstr)
    fnameDict[vstr] = grib2ToNetCDF(fvo, vstr, plev)
    
  return fnameDict

#==========================================================#

def slevDataFiles( varList, dtime ):
  y,m,d,h = timeStamp(dtime)
  
  fnameDict = dict()
  for vstr in varList:
    fv, pv = singleLevelFileAndPath(vstr, y, m, d, h, dtime )
    fvo    = ddDataFile(fv, pv, vstr)
    fnameDict[vstr] = grib2ToNetCDF(fvo, vstr) 
    
  return fnameDict

#==========================================================#

def tinvDataFiles( varList, dtime ):
  y,m,d,h = timeStamp(dtime)
  
  fnameDict = dict()
  for vstr in varList:
    fv, pv = timeInvariantLevelFileAndPath( vstr, y, m, d, h )
    fvo    = ddDataFile(fv, pv, vstr)
    fnameDict[vstr] = grib2ToNetCDF(fvo, vstr)
    
  return fnameDict

#==========================================================#
