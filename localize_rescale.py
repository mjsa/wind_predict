#!/usr/bin/env python3
import sys
import numpy as np

#==========================================================#

def readDictFile( filename, verbose=False):
  if (verbose):
    print(' Read filename {} '.format(filename))
  
  # NOTE: dat must be closed to avoid leaking file descriptors.
  try:
    dat = np.load(filename, allow_pickle=True)
    DataDict = dict(dat)
    dat.close()
  except IOError as e:
    print('Error reading file {0}: {1}'.format(filename, e.strerror))
    sys.exit(e.errno)

  return DataDict

#==========================================================#

def scalingFactor( Uref, Zref, loc, wdir ):
  
  f1d    = './U1D-{:1s}/Umag_{:03d}.dat'.format(loc,wdir)
  z1, u1 = np.loadtxt(f1d, usecols=(0,1), unpack=True) 
  dz  = np.mean( (z1[1:]-z1[:-1]) )
  idr = ( np.abs(Zref-z1) <= dz )
  
  Upre = np.mean( u1[idr] )
  
  C = Uref/Upre
  
  #print(' Ui = {}, Uref = {} '.format(Upre, Uref))
  #print(' Scaling factor C = {}'.format(C))
  
  return C

#==========================================================#

def gustDataDict( Cs, loc, wdir ):
  
  mLS = ['N05','N06','N07','N08','N09','N10']
  mLG = ['ID01','ID02','ID03','ID04','ID05','ID06','ID07','ID08','ID09']
  
  if(loc =='S'): maskKeys = mLS
  elif(loc=='G'): maskKeys = mLG#; sys.exit('Greece not implemented yet. Exiting ...')
  else: sys.exit(1)
  
  directory = './UGUST-{:1s}'.format(loc)
  
  gDict = dict()
  for m in maskKeys:
    fg = '{}/UG_{:3s}_{:03d}.npz'.format(directory,m,wdir)
    
    # Feed gust data dict within file for each mask into gDict
    # Mask name will be used as the key.
    datDict  = readDictFile( fg )
    datDict  = scaleGustValues( Cs, datDict )
    gDict[m] = datDict.copy(); datDict = None
    
  return maskKeys, gDict 

#==========================================================#

def scaleGustValues( Cs, fDict ):
  valKeys = ['mean','max','std','p90','p95','p99']

  for k in valKeys:
    fDict[k] *= Cs
    
  hist = fDict['hist']
  hist[1] *= Cs
  fDict['hist'] = hist
  
  return fDict


#==========================================================#
def localizeMeteoAndReport(nhours, hs, pr, rh_2m , tmax_2m, tmin_2m, t_2m):
  
  nbins = int( (np.max(hs)-np.min(hs))/150. ) # 4 elevation levels
  tmax  = np.zeros( nbins )
  tmin  = np.zeros( nbins )
  tmean = np.zeros( nbins )

  #==========================================================#
  # Relative humidity 
  rhbins    = [0.,25.,50.,75.,100.] # relhum bins
  wrhb = 25.                          # bin width
  prh = np.zeros( (nbins, len(rhbins)-1) )

  #==========================================================#
  # Total cumulative precipitation [pr]
  # Convert the cumulative total precipitation mm measure into mm per hour
  pr /= nhours
  prbins = np.array([0., 0.05, 2., 4., 6., 20. ]) # mm per hour bin bounds
  wprb   = (prbins[1:]-prbins[:-1])
  ppr = np.zeros( (nbins, len(prbins)-1) )

  #==========================================================#
  # Elevation histogram which divides the area according to its surface height
  phs, hsbins = np.histogram( hs, bins=nbins )
  #print(' Surface height bins = {}'.format(hsbins))
  meteo_info = list()

  for i in range(nbins):
    idm = (hs>=hsbins[i])*(hs<=hsbins[i+1])
  
    tmax[i] = np.max( tmax_2m[idm] )-273.15
    tmin[i] = np.min( tmin_2m[idm] )-273.15
    tmean[i]= np.mean(t_2m[idm])-273.15
  
    prh[i,:], rhbins = np.histogram(rh_2m[idm], bins=rhbins, density=True)
    prh[i,:] *= (wrhb*100.)  # rel hum area distribution in percent (%) of total
  
    ppr[i,:], prbins = np.histogram(pr[idm], bins=prbins, density=True )
    ppr[i,:] = ppr[i,:]*wprb[:]*100. # prec per hour probability in percent
  

    td='{:9.1f},{:8.1f},{:9.1f}'.format(tmean[i], tmax[i], tmin[i])
    rd=',{:16.0f},{:7.0f},{:7.0f},{:8.0f}'.format(prh[i,0], prh[i,1], prh[i,2], prh[i,3])
    pd=',{:18.0f},{:8.0f},{:8.0f},{:8.0f}'.format(ppr[i,1], ppr[i,2], ppr[i,3], ppr[i,4])
  
    #print(sd)
    meteo_info.append( td+rd+pd+'\n')

  meteo_header= '#    UTM-X ,      UTM-Y , Tavg (C), Tmax (C), Tmin (C),'
  meteo_header+=' P(RH)(%): 0-25%, 25-50%, 50-75%, 75-100%,'
  meteo_header+=' P(pr)(%) 0-2 mm/h, 2-4 mm/h, 4-6 mm/h, +6 mm/h \n'


  '''
  tmax  = np.array( [ np.percentile( tmax_2m, 95. ), np.percentile( tmax_2m, 5. )] )-273.15
  tmin  = np.array( [ np.percentile( tmin_2m, 95. ), np.percentile( tmin_2m, 5. )] )-273.15
  tmean = np.mean( t_2m )-273.15
  '''
  
  # Output also surface height bins (hsbins) which is needed in assembling the meteo report.

  return meteo_info, meteo_header, hsbins



#==========================================================#

def scaleGustsAndReport( Uref, Zref, loc, wdir, verbose=False):
  '''
  Perform scaling and compile the gust report for output
  '''
  Cu = scalingFactor( Uref, Zref, loc , wdir )
  if( verbose ):
    print('# Scaling factor for wind speed: Cu = {:0.2f}'.format(Cu))

  keys, GDict = gustDataDict( Cu, loc, wdir )

  # Latitudes and longitudes that will be returned for meteo localization.
  utm_e = np.zeros(len(keys)); utm_n = np.zeros(len(keys))


  # == Output the wind information ==
  w_report = '#    UTM-X ,     UTM-Y , Max (m/s), P99 (m/s), Mean (m/s), Std (m/s)\n'
  for i,k in enumerate(keys):
    D = GDict[k]
  
    utm = D['utm']
    
    s = '{0:.3f}, {1:.3f},      {2:4.1f},      {3:4.1f},       {4:4.1f},      {5:4.1f}\n'\
    .format(utm[1],utm[0],D['max'][0],D['p99'][0],D['mean'][0],D['std'][0])
    D = None
    w_report += s
    
    utm_e[i], utm_n[i] = utm[1], utm[0]
    
  return w_report , utm_e, utm_n

#==========================================================#

def utmToLatLng(zone, easting, northing, northernHemisphere=True):
    if not northernHemisphere:
        northing = 10000000. - northing

    a = 6378137.; e = 0.081819191; e1sq = 0.006739497; k0 = 0.9996

    arc = northing / k0
    mu = arc / (a * (1.-np.power(e,2) / 4.0 - 3.*np.power(e,4)/64. - 5.*np.power(e,6)/256.))

    ei = (1.-np.power((1.-e*e), (1./2.0))) / (1.+np.power((1.- e*e), (1./2.)))

    ca = 3.*ei/2. - 27.*np.power(ei,3) / 32.0

    cb = 21.*np.power(ei,2) / 16. - 55.*np.power(ei,4) / 32.
    cc = 151. *np.power(ei,3) / 96.
    cd = 1097.*np.power(ei,4) / 512.
    phi1 = mu + ca * np.sin(2.*mu) + cb*np.sin(4.*mu) + cc*np.sin(6.*mu) + cd*np.sin(8.*mu)

    n0 = a / np.power((1 - np.power((e * np.sin(phi1)),2)), (1./2.0))

    r0 = a * (1.-e*e) / np.power((1.-np.power((e*np.sin(phi1)),2)), (3./2.0))
    fact1 = n0 * np.tan(phi1) / r0

    _a1 = 500000. - easting
    dd0 = _a1 / (n0 * k0)
    fact2 = dd0 * dd0 / 2.

    t0 = np.power(np.tan(phi1), 2)
    Q0 = e1sq * np.power(np.cos(phi1), 2)
    fact3 = (5. + 3.*t0 + 10.*Q0 - 4.*Q0*Q0 - 9.*e1sq) * np.power(dd0, 4) / 24.

    fact4 = (61. + 90.*t0 + 298.*Q0 + 45.*t0*t0 - 252.*e1sq - 3.*Q0*Q0) * np.power(dd0,6) / 720.

    lof1 = _a1 / (n0*k0)
    lof2 = (1. + 2.*t0 + Q0) * np.power(dd0, 3) / 6.0
    lof3 = (5. - 2.*Q0 + 28.*t0 - 3.*np.power(Q0,2) + 8.*e1sq + 24.*np.power(t0,2))*np.power(dd0,5) / 120.
    _a2 = (lof1 - lof2 + lof3) / np.cos(phi1)
    _a3 = _a2*180. / np.pi

    latitude = 180. * (phi1 - fact1 * (fact2 + fact3 + fact4)) / np.pi

    if not northernHemisphere:
        latitude = -latitude

    longitude = ((zone > 0) and (6.*zone - 183.) or 3.) - _a3

    return (latitude, longitude)

#==========================================================#
