#!/usr/bin/env python3
import sys
import numpy as np
import netCDF4 as nc

''' 
Description: Python module for reading NetCDF files from ICON-EU forecast model
and computing the reference height of the chosen pressure level and for
extracting the necessary wind speed information from that elevation. 

Author: Mikko Auvinen
        mikko.auvinen@fmi.fi 
        Finnish Meteorological Institute
'''

#==========================================================#

def asciiEncode(uList, uStr):
  n = len(uList)
  if(n > 0):
    uList = list(uList)  # This might be a tuple coming in
    for i in range(len(uList)):
      if isinstance(uList[i], bytes): uList[i] = uList[i].decode()
  else:
    print(' Dictionary {} has zero length. Exiting ...'.format(uStr))
    sys.exit(1)

  return uList

#==========================================================#

def netcdfDataset(filename, verbose=True):
  # Create Dataset
  ds = nc.Dataset(filename)

  # Generate a list of variables and independent variables contained in the file.
  varList = asciiEncode(ds.variables.keys(), 'Variables')
  dimList = asciiEncode(ds.dimensions.keys(), 'Dimensions')

  if(verbose):
    print(' Variable List : {} '.format(varList))
    print(' Dimension List : {} '.format(dimList))

  return ds, varList, dimList
#==========================================================#

def readVariableFromDataset(varStr, ds, cl=1 ):
  if( varStr in ds.variables.keys() ):

    vdims = asciiEncode(ds.variables[varStr].dimensions, ' Variable dimensions ')

    if( len(vdims) == 4 ):
      var = ds.variables[varStr][:,::cl,::cl,::cl]
    elif( len(vdims) == 3 and 'time' not in vdims ):
      var = ds.variables[varStr][::cl,::cl,::cl]
    elif( len(vdims) == 3 and 'time' in vdims ):
      var = ds.variables[varStr][:,::cl,::cl]
    elif( len(vdims) == 2 and 'time' not in vdims ):
      var = ds.variables[varStr][::cl,::cl]
    elif( len(vdims) == 2 and 'time' in vdims ):
      #print(' {} {} '.format(varStr, ds.variables[varStr][:].shape ))
      var = ds.variables[varStr][:,::cl]
    elif( len(vdims) == 1 and 'time' in vdims ):
      var = ds.variables[varStr]
    else:
      var = ds.variables[varStr][::cl]

    # Load the independent variables and wrap them into a dict
    dDict = dict()
    for dname in vdims:
      dData = ds.variables[dname][:]
      if( 'time' in dname ): dDict[dname] = dData
      else:                  dDict[dname] = dData[::cl]
      dData = None

  else:
    sys.exit(' Variable {} not in list {}.'.format(varStr, ds.variables.keys()))

  return var, dDict

#==========================================================#

def readLatLonData( varStr, filename, latb, lonb ):
  ds, varList, paramList = netcdfDataset(filename, False)
  var, varDict = readVariableFromDataset(varStr, ds, cl=1 )

  lat1d = varDict['lat']; lon1d = varDict['lon']
  dlat = len(lat1d); dlon = len(lon1d)
  if( np.mean(lon1d) > 300.): lon1d -= 360.
  
  var = var.reshape( (dlat,dlon) )
  
  vloc, Xloc, Yloc = isolateLoc( var, lat1d, lon1d, latb, lonb )
  var = lat1d = lon1d = None
  
  return vloc, Xloc, Yloc

#==========================================================#

def isolateLoc( var, lat1d, lon1d, latb, lonb ):
  Xlon,Ylat = np.meshgrid(lon1d,lat1d)
  nlon = np.count_nonzero((lon1d > lonb[0])*(lon1d < lonb[1]))
  nlat = np.count_nonzero((lat1d > latb[0])*(lat1d < latb[1]))
  ids = (Xlon>lonb[0])*(Xlon<lonb[1]) * (Ylat>latb[0])*(Ylat<latb[1])
  
  varl = var[ids].reshape( (nlat,nlon) )
  Xl   = Xlon[ids].reshape( (nlat,nlon) )
  Yl   = Ylat[ids].reshape( (nlat,nlon) )

  return varl, Xl, Yl

#==========================================================#

def windData( ux, vy ):
  umag   = np.sqrt( ux**2 + vy**2 )
  wdir   = np.arctan2( vy , (ux+1.e-5) ) * (180./np.pi)
  # Process the wind direction 
  
  wdir *= -1.; wdir += 270.
  
  ids = (wdir >= 360.)
  wdir[ids] -= 360.
  ids = None

  return umag, wdir

#==========================================================#

def dzPressureLevels(t2, t1, p2, p1, h1):
  '''
  Compute the elevation difference between two pressure levels
  and extract the relevant wind speed from that level.
  '''
  R = 287.
  g = 9.81
  tmean = 0.5*(t2 + t1)
  
  # dZ is the distance from the groud to the chosen pressure level. 
  # This distance is immediately comparable with the z-coord of the LES model.
  dZ     = R*tmean/g*np.log( p1/float(p2*100.) ) 
  
  Zp     = dZ + h1  # <-- Real height above the sea level (only FYI at this point).
  #Zref   = np.percentile( (Zp - ZminModel), 50 )
  #print(' Real reference height, Zreal = {:4.1f} m'.format(np.mean(Zp)))
  Zmodel = np.percentile(dZ, 80)

  return Zmodel
#==========================================================#

def latlonBounds( location ):
  # Lon and lat locales. [S]: Spanish, [G]: Greek pilot sites.
  if( location == 'S' ):  # Spain
    lonbounds = np.array( [ -3.2, -2.2 ] )
    latbounds = np.array( [ 40.10, 41.10 ] )
    
    #lonbounds = np.array( [ -6.,  0.0 ] )
    #latbounds = np.array( [ 37.0, 43. ] )
    
  elif( location == 'G'): # Greece 39.77, 21.18.
    lonbounds = np.array( [ 20.93, 21.43 ] )
    latbounds = np.array( [ 39.52, 40.02 ] ) 
  else:   # Helsinki is at 60.67, 24.93. This is only for testing.
    lonbounds = np.array( [ 24.23 , 25.63  ] )
    latbounds = np.array( [ 59.37 , 60.97  ] ) 

  return latbounds, lonbounds

#==========================================================#
